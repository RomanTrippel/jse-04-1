package ru.trippel.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private static Date parseDate(String date) throws ParseException {
        return simpleDateFormat.parse(date);
    }

}