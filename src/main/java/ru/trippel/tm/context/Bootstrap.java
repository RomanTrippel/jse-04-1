package ru.trippel.tm.context;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import ru.trippel.tm.enumeration.TerminalCommand;
import ru.trippel.tm.repository.ProjectRepository;
import ru.trippel.tm.repository.TaskRepository;
import ru.trippel.tm.service.ProjectService;
import ru.trippel.tm.service.TaskService;
import ru.trippel.tm.view.*;
import java.io.IOException;
import java.util.List;
import static ru.trippel.tm.enumeration.TerminalCommand.EXIT;

public class Bootstrap {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    public void execute(String s) throws IOException {
        TerminalCommand command = null;
        try {
            command = TerminalCommand.valueOf(s);
        } catch (Exception e) {
            BootstrapView.printError();
            return;
        }
        switch (command) {
            case HELP:
                HelpView.printCommandList();
                break;
            case PROJECT_CREATE:
                projectCreate();
                break;
            case PROJECT_VIEW:
                projectView();
                break;
            case PROJECT_EDIT:
                projectEdit();
                break;
            case PROJECT_REMOVE:
                projectRemove();
                break;
            case PROJECT_ATTACHTASK:
                attach();
                break;
            case PROJECT_VIEWTASK:
                projectViewAttached();
                break;
            case TASK_CREATE:
                taskCreate();
                break;
            case TASK_VIEW:
                taskView();
                break;
            case TASK_EDIT:
                taskEdit();
                break;
            case TASK_REMOVE:
                taskRemove();
                break;
        }
    }

    public void init() throws IOException {
        BootstrapView.printWelcome();
        String s="";
        while (true) {
            s = KeyboardView.read();
            if (s.equals(EXIT.name())) end();
            execute(s);
        }
    }

    private void projectViewAttached() throws IOException {
        String projectId;
        List<Task> taskList = taskService.findAll();
        projectId = ProjectView.findId(projectService.findAll());
        for (int i = 0; i < taskList.size() ; i++) {
            if (taskList.get(i).getProjectId().equals(projectId)) {
                System.out.println(taskList.get(i).getName());
            }
        }
    }

    private void attach() throws IOException {
        Task task;
        String projectId;
        String taskId;
        projectId = ProjectView.findId(projectService.findAll());
        taskId = TaskView.findId(taskRepository.findAll());
        task = taskService.findOne(taskId);
        task.setProjectId(projectId);
    }

    private void projectEdit() throws IOException {
        Project project;
        String id;
        String newName;
        id = ProjectView.findId(projectService.findAll());
        newName = ProjectView.merge();
        project = projectService.findOne(id);
        project.setName(newName);
        projectService.merge(project);
    }

    private void projectCreate() throws IOException {
        Project project = new Project();
        String name = ProjectView.create();
        project.setName(name);
        projectService.persist(project);
        ProjectView.add(project);
    }

    private void projectView() {
        List<Project> projectList = projectService.findAll();
        ProjectView.printList(projectService.findAll());
    }

    private void projectRemove() throws IOException {
        String projectId;
        List<Task> taskList = taskService.findAll();
        projectId = ProjectView.findId(projectService.findAll());
        projectService.remove(projectId);
        for (int i = 0; i < taskList.size() ; i++) {
            if (taskList.get(i).getProjectId().equals(projectId)) {
                taskService.remove(taskList.get(i).getTaskId());
            }
        }
    }

    private void taskEdit() throws IOException {
        Task task;
        String id;
        String newName;
        id = TaskView.findId(taskService.findAll());
        newName = TaskView.merge();
        task = taskService.findOne(id);
        System.out.println(task.getName() + task.getTaskId());
        task.setName(newName);
        taskService.merge(task);
    }

    private void taskRemove() throws IOException {
        String id;
        id = TaskView.findId(taskService.findAll());
        taskService.remove(id);
    }

    private void taskCreate() throws IOException {
        Task task = new Task();
        String name = TaskView.create();
        task.setName(name);
        taskService.persist(task);
        TaskView.add(task);
    }

    private void taskView() {
        List<Task> taskList = taskService.findAll();
        TaskView.printList(taskService.findAll());
    }

    private void end() {
        BootstrapView.printGoodbye();
        System.exit(0);
    }

}
