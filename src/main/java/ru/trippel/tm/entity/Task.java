package ru.trippel.tm.entity;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class Task {

    private String taskId = UUID.randomUUID().toString().replace("-", "");

    private String projectId = "";

    private String name = "";

    private String description = "";

    private Date dateStart = new Date();

    private Date dateFinish = new Date(2100);

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(taskId, task.taskId) &&
                Objects.equals(projectId, task.projectId) &&
                Objects.equals(name, task.name) &&
                Objects.equals(description, task.description) &&
                Objects.equals(dateStart, task.dateStart) &&
                Objects.equals(dateFinish, task.dateFinish);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskId, projectId, name, description, dateStart, dateFinish);
    }

}