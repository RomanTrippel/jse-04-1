package ru.trippel.tm.enumeration;

public enum TerminalCommand {

    PROJECT_CREATE("Create new project"),
    PROJECT_VIEW("Show all project"),
    PROJECT_EDIT("Edit project"),
    PROJECT_REMOVE("Remove project"),
    PROJECT_ATTACHTASK("Attach Task to Project"),
    PROJECT_VIEWTASK("View all attached Task"),
    TASK_CREATE("Create new Task"),
    TASK_VIEW("Show all project"),
    TASK_EDIT("Edit project"),
    TASK_REMOVE("Remove project"),
    HELP("List commands"),
    EXIT("Exit the application");

    private String description = "";

    TerminalCommand(String description) {
     this.description = description;
    }

    public String getDescription() {
        return description;
    }

}