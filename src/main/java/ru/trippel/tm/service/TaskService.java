package ru.trippel.tm.service;

import ru.trippel.tm.entity.Task;
import ru.trippel.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findOne(String id) throws NullPointerException {
        if (id == null) throw new NullPointerException();
        if (id.isEmpty()) return null;
        return taskRepository.findOne(id);
    }

    public Task persist(Task task) throws NullPointerException {
        if (task == null) throw new NullPointerException();
        String id = task.getTaskId();
        if (id.isEmpty()) return null;
        return taskRepository.persist(task);
    }

    public Task merge(Task task) throws NullPointerException {
        if (task == null) throw  new NullPointerException();
        String id = task.getTaskId();
        if (id.isEmpty()) return null;
        return taskRepository.merge(task);
    }

    public Task remove(String id) throws NullPointerException {
        if (id == null) throw new NullPointerException();
        if (id.isEmpty()) return null;
        return taskRepository.remove(id);
    }

}
