package ru.trippel.tm.view;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProjectView {

    public static void printList(List<Project> projectList) {
        Project project;
        System.out.println("Project List:");
        for (int i = 0; i < projectList.size(); i++) {
            project = projectList.get(i);
            System.out.println(i+1 + ". " + project.getProjectId() + " - " + project.getName());
        }
    }

    public static String create() throws IOException {
        System.out.println("Enter project name.");
        return KeyboardView.read();
    }

    public static void add(Project project) {
        System.out.println("Project \"" + project.getProjectId() + " - " + project.getName() + "\" added! - ");

    }

    public static String merge() throws IOException {
        System.out.println("Enter new Name:");
        String name = KeyboardView.read();
        return name;
    }

    public static String findId(List<Project> projectList) throws IOException {
        int projectNum = -1;
        String id;
        printList(projectList);
        System.out.println("Enter a project number to Edit:");
        projectNum +=  Integer.parseInt(KeyboardView.read());
        id = projectList.get(projectNum).getProjectId();
        return id;
    }

}
