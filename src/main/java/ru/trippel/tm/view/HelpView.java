package ru.trippel.tm.view;

import ru.trippel.tm.enumeration.TerminalCommand;

public class HelpView {

    private static TerminalCommand[] command = TerminalCommand.values();

    public static void printCommandList() {

        System.out.println("Command list:");
        for (int i = 0; i < command.length; i++) {
            System.out.println(command[i] + " --- " + command[i].getDescription());
        }
    }

}
