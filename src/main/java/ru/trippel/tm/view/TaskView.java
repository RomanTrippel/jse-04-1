package ru.trippel.tm.view;

import ru.trippel.tm.entity.Project;
import ru.trippel.tm.entity.Task;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TaskView {

    public static String create() throws IOException {
        System.out.println("Enter task name.");
        return KeyboardView.read();
    }

    public static void add(Task task) {
        System.out.println("Task \"" + task.getTaskId() + " - " + task.getName() + "\" added! - ");

    }

    public static void printList(List<Task> taskList) {
        Task task;
        System.out.println("Task List:");
        for (int i = 0; i < taskList.size(); i++) {
            task = taskList.get(i);
            System.out.println(i+1 + ". " + task.getTaskId() + " - " + task.getName());
        }
    }

    public static String findId(List<Task> taskList) throws IOException {
        int taskNum = -1;
        String id;
        printList(taskList);
        System.out.println("Enter a task number to Edit:");
        taskNum +=  Integer.parseInt(KeyboardView.read());
        id = taskList.get(taskNum).getTaskId();
        return id;
    }

    public static String merge() throws IOException {
        System.out.println("Enter new Name:");
        String name = KeyboardView.read();
        return name;
    }

}