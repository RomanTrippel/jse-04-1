package ru.trippel.tm.view;

public class BootstrapView {

    public static void printWelcome(){
        System.out.println("~~~~~Welcome to task manager~~~~~" +
                "\nEnter a command. For help, type \"HELP\".");
    }

    public static void printGoodbye(){
        System.out.println("~~~~~See you soon. Come again!~~~~~");
    }

    public static void printError() {
        System.out.println("Invalid request, try again");
    }

}