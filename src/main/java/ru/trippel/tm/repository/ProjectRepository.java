package ru.trippel.tm.repository;

import ru.trippel.tm.entity.Project;

import java.util.*;

public class ProjectRepository extends AbstractRepository<Project> {

    @Override
    public List<Project> findAll() {
        return new LinkedList<Project>(map.values());
    }

    @Override
    public Project findOne(String id) {
        return map.get(id);
    }

    @Override
    public Project persist(Project project) {
        String projectId = project.getProjectId();
        if (map.containsKey(projectId)) return null;
        return map.put(projectId,project);
    }

    @Override
    public Project merge(Project project) {
        String projectId = project.getProjectId();
        return map.merge(projectId, project, (oldProject, newProject) -> newProject);
    }

    @Override
    public Project remove(String id) {
        return map.remove(id);
    }

    @Override
    public void removeAll() {
        map.clear();
    }

}
